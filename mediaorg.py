#!/usr/bin/env python

from optparse import OptionParser
import os.path
import os
import sys
import shutil
import hashlib
from pprint import PrettyPrinter

import JPEGInfo
import AVIInfo
import ExifToolInfo 
import MediaInfo

def get_file_hash(filename):
  chksum = hashlib.sha1()
  fp = open(filename)
  databuf = fp.read(10000000)

  while len(databuf) > 0:
    chksum.update(databuf)
    databuf = fp.read(10000000)

  return chksum.digest()

def hexdump(strval):
  outval = ""
  for char in strval:
    outval += "%02x" % (ord(char))

  return outval

if __name__ == "__main__":

  parser = OptionParser(usage="%prog [options] <src file> [src file2.. [ src file3 [..]]]")

  parser.add_option("-d", "--destination-directory", dest="destdir",
                    default=".", help="Set the destination directory")

  parser.add_option("-l", "--list", dest="listinfo", action="store_true",
                    default=False, help="Only list file info")

  parser.add_option("-p", "--rename-pattern", dest='rename_pat',
                    default="%(year)d/%(month)02d/%(year)04d%(month)02d%(day)02d_%(hour)02d%(minute)02d%(second)02d_%(filename)s",
                    help="Use the given rename pattern")

  parser.add_option("-r", "--show-rename", dest="show_rename",
                    action="store_true",
                    default=False, help="Display the results of rename given in rename-pattern")

  parser.add_option("-R", "--perform-rename", dest="enable_rename",
                    action="store_true",
                    default=False, help="Actually perform the rename given in rename-pattern")

  parser.add_option("-V", "--validate-duplicates", dest="validate_duplicates",
                    action="store_true", default=False,
                    help="When duplicate filenames are detected, run a checksum validation on the files")

  parser.add_option("-U", "--unlink-equivalent", dest="unlink_equivalent",
                    action="store_true", default=False,
                    help="Unlink the duplicate source files when found")
  
  parser.add_option("-M", "--use-modify-date", dest="use_modify_date",
                    action="store_true", default=False,
                    help="If now real sources of time are found, fall back to modify date.")

  (options, args) = parser.parse_args()

  if len(args) < 1:
    parser.error("Must supply at least one source files")

  for src in args:

    try:
      fileInfo = MediaInfo.info_from_file(src, options.use_modify_date)
      if options.listinfo:
         pp = PrettyPrinter()
         pp.pprint(fileInfo.info())

      if options.show_rename or options.enable_rename:
        try:
          newname = (options.destdir + "/" + options.rename_pat) % fileInfo.info()

          if options.show_rename:
            print "Renaming '%s' -> '%s'" % (src, newname)

          if os.path.exists(newname):
            if options.validate_duplicates:
              file1_hash = get_file_hash(src)
              file2_hash = get_file_hash(newname)
              #print "1: %s\n2: %s" % (hexdump(file1_hash), hexdump(file2_hash))
              if file1_hash == file2_hash:
                print "'%s' already exists and has equivalent data" % (newname)
                if options.unlink_equivalent:
                  print "Unlinking duplicate file %s" % (src)
                  os.unlink(src)
              else:
                print "'%s' already exists but has different data" % (newname)
            else:
              print "'%s' already exists" % (newname)

          elif options.enable_rename:
            # Ensure the parent directory exists
            parentpath = os.path.dirname(newname)
            if not os.path.exists(parentpath):
              os.makedirs(parentpath)
            
            shutil.move(src, newname)
        except Exception as e:
          print "Failed to rename '%s': %s" % (src, e)

    # Catch-all so that we don't kill the whole process
    except Exception as e:
      print "Error processing %s: %s" % (src, e)



