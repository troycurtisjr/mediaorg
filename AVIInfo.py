#!/usr/bin/env python

import MediaInfo
import kaa.metadata
from datetime import datetime

class AVIInfo(MediaInfo.BaseInfo):

  def __init__(self, path=None):
    MediaInfo.BaseInfo.__init__(self, path)

  def read(self, path):
    MediaInfo.BaseInfo.read(self, path)

    if path == None:
      return False

    mediaobj = kaa.metadata.parse(path)

    if mediaobj == None:
      print "Failed to parse metadata from " + path
      return False

    self._info.update(mediaobj.convert())

    if self._info['timestamp']:
      ts = datetime.fromtimestamp(self._info['timestamp'])
      self.set_timestamp(ts)

    return self._info['mime'] != None

MediaInfo.Registry.register("video/x-msvideo", AVIInfo())
MediaInfo.Registry.register("video/avi", AVIInfo())
