#!/usr/bin/env python

from pprint import PrettyPrinter
import sys
import os.path
from datetime import datetime
from copy import deepcopy
from mimetypes import guess_type

class Registry:
  _registry = dict()

  @staticmethod
  def register(mimetype, obj):
    Registry._registry[mimetype] = deepcopy(obj)

  @staticmethod
  def getinfoobj(mimetype):
    if (mimetype not in Registry._registry):
      if ('default' not in Registry._registry):
        print "No handler for mimetype: '%s'\n" % mimetype
        return None
      else:
        return deepcopy(Registry._registry['default'])

    return deepcopy(Registry._registry[mimetype])


class BaseInfo:

  def __init__(self, path = None):

    self.read(path)

  def read(self, path = None):
    self._info = dict()
    self._info['path']      = None
    self._info['timestamp'] = None
    self._info['filename']  = None
    self._info['dirname']   = None
    self._info['mime']      = None


    if (path):
      self._info['path']     = os.path.abspath(path)
      self._info['filename'] = os.path.basename(self._info['path'])
      self._info['dirname']  = os.path.dirname(self._info['path'])
      self._info['mime']     = detect_filetype(path)['mime']

    return self._info['mime'] != None 

  def set_timestamp(self, ts):
    self._info['timestamp'] = ts

    if self._info['timestamp']:
      self._info['year']   = self._info['timestamp'].year
      self._info['month']  = self._info['timestamp'].month
      self._info['day']    = self._info['timestamp'].day
      self._info['hour']   = self._info['timestamp'].hour
      self._info['minute'] = self._info['timestamp'].minute
      self._info['second'] = self._info['timestamp'].second

  def media_type(self):
    return self._info['mime']

  def timestamp(self):
    return self._info['timestamp']

  def info(self):
    return self._info

  def full_path(self): 
    return self._info['path']

  def search_alternate_timestamp(self, listOfTags):
    for tag in listOfTags:
      if tag in self._info:
        format_str = "%Y:%m:%d %H:%M:%S"
        expected_len = len(format_str) + 2
        try:
          self.set_timestamp(datetime.strptime(
                                      self._info[tag][0:expected_len],
                                      format_str ))
          break
        # Just eat parsing failure
        except ValueError:
          print "Cannot convert '%s' using '%s'" % ( self._info[tag],
                                                     format_str )

def detect_filetype(path):
  detectedType = guess_type(path)
  retType = dict()
  retType['mime'] = detectedType[0] 
  retType['encoding'] = detectedType[1]
  return retType
  
def info_from_file(path, fallback_to_modify_time=False):
  detectedType = detect_filetype(path)

  fileInfo = Registry.getinfoobj(detectedType['mime']) 
  fileInfo.read(path)

  if fallback_to_modify_time:
    tmpInfo = fileInfo.info()
    if not tmpInfo['timestamp']:
      fileInfo.search_alternate_timestamp((
        'File:FileModifyDate',
        u'File:FileModifyDate',
        ))

  return fileInfo

if __name__ == "__main__":
    
  pp = PrettyPrinter()
  for path in sys.argv[1:]:
    media_info = info_from_file(path)
    pp.pprint(media_info.info())

