#!/usr/bin/env python

import MediaInfo
import exifread
from datetime import datetime

class JPEGInfo(MediaInfo.BaseInfo):

  def __init__(self, path = None):
    MediaInfo.BaseInfo.__init__(self, path)

  def read(self, path):
    MediaInfo.BaseInfo.read(self, path)

    if path == None:
      return False

    fp = open(path, "r")
    tags = exifread.process_file(fp)
    fp.close()

    if len(tags) == 0:
      print "Failed to read metadata from " + path
      return False

    self._info.update(tags)

    timestampTags = ( 'EXIF DateTimeOriginal',
                      'EXIF DateTimeDigitized' )
    for tag in timestampTags:
      if tag in self._info:
        format_str = "%Y:%m:%d %H:%M:%S"
        try:
          self.set_timestamp(datetime.strptime(
                                      self._info[tag].values,
                                      format_str ))
        # Just eat parsing failure
        except ValueError:
          print "Cannot convert '%s' using '%s'" % ( self._info[tag].values,
                                                     format_str )
        break


    return self._info['mime'] != None

#MediaInfo.Registry.register("image/jpeg", JPEGInfo())
#MediaInfo.Registry.register("model/vnd.mts", JPEGInfo())
