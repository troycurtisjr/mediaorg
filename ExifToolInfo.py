#!/usr/bin/env python

import MediaInfo
import exiftool
from datetime import datetime

class ExifToolInfo(MediaInfo.BaseInfo):

  def __init__(self, path=None):
    MediaInfo.BaseInfo.__init__(self, path)

  def read(self, path):
    MediaInfo.BaseInfo.read(self, path)

    if path == None:
      return False

    tool = exiftool.ExifTool()
    tool.start()
    metatags = tool.get_metadata(path) 
    tool.terminate()

    if     metatags == None \
        or len(metatags) == 0:
      print "Failed to parse metadata from " + path
      return False

    self._info.update(metatags)

    self.search_alternate_timestamp(
        (  'EXIF:CreateDate', 
          u'EXIF:CreateDate', 
           'EXIF:DateTimeOriginal', 
          u'EXIF:DateTimeOriginal', 
           'EXIF:DateTimeDigitized', 
          u'EXIF:DateTimeDigitized', 
           'H264:DateTimeOriginal',
          u'H264:DateTimeOriginal',
           'QuickTime:TrackCreateDate',
          u'QuickTime:TrackCreateDate',
           'QuickTime:CreateDate',
          u'QuickTime:CreateDate'
          ))

    return self._info['mime'] != None

MediaInfo.Registry.register("image/jpeg", ExifToolInfo())
MediaInfo.Registry.register("model/vnd.mts", ExifToolInfo())
MediaInfo.Registry.register("default", ExifToolInfo())
