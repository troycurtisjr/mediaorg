Media Organizer
===============

This is a really simple application which just renames a directory of media
files into a new directory structure based on the embedded time information.  A
lot of effort is taken to not lose any files.

INSTALL
=======

Currently there is no install, and it is just run out of the directory.
Perhaps eventually I'll include a setuptool based package.
